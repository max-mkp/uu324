<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");?><?

if (\Bitrix\Main\Loader::includeModule('uu324.task')){
//    вывод объектов
    echo 'Objects: <br><br>';
//    вывод пользователя относительно задачи
    $task = Uu324\Task\Tasks\TasksTable::getList([
        'select' => ['*', 'USERS'],
        'limit' => '1'
    ])->fetchObject();

    if (!$task) die();

    echo 'Task "' . $task->getTitle() . '": '
        . $task->getUsers()->getFirstName() . ' '
        . $task->getUsers()->getSecondName() . ' '
        . $task->getUsers()->getLastName()
        . ' (' . $task->getUsers()->getLogin() . ')';

    echo '<br><br>';

    $id = $task->getUser();

//  вывод задач относительно пользователя
    $user = Uu324\Task\Users\UsersTable::getByPrimary($id, [
        'select' => ['*', 'TASKS']
    ])->fetchObject();

    echo 'User ' . $user->getFirstName() . ' '
        . $user->getSecondName() . ' '
        . $user->getLastName()
        . ' (' . $user->getLogin() . ') <br> Tasks: <ul>';
    foreach ($user->getTasks() as $task)
    {
        echo '<li>'.$task->getTitle().'</li>';
    }
    echo '</ul>';

//вывод коллекций
    echo '<br><br> Collections: <br><br>';

//    вывод пользователя относительно задачи
    $taskCollection = Uu324\Task\Tasks\TasksTable::getList([
        'select' => ['*', 'USERS']
    ])->fetchCollection();
    foreach ($taskCollection as $task) {
        echo 'Task "' . $task->getTitle() . '": '
            . $task->getUsers()->getFirstName() . ' '
            . $task->getUsers()->getSecondName() . ' '
            . $task->getUsers()->getLastName()
            . ' (' . $task->getUsers()->getLogin() . ')';

        echo '<br>';
    }
    echo '<br>';

//  вывод задач относительно пользователя
    $userCollection = Uu324\Task\Users\UsersTable::getByPrimary($id, [
        'select' => ['*', 'TASKS']
    ])->fetchCollection();
    foreach ($userCollection as $user) {
        echo 'User ' . $user->getFirstName() . ' '
            . $user->getSecondName() . ' '
            . $user->getLastName()
            . ' (' . $user->getLogin() . ') <br> Tasks: <ul>';

        foreach ($user->getTasks() as $task) {
            echo '<li>' . $task->getTitle() . '</li>';
        }
        echo '</ul><br><br>';
    }
}
?>