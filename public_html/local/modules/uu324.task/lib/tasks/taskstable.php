<?php
namespace Uu324\Task\Tasks;

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ORM\Fields\Relations\Reference;
use Bitrix\Main\ORM\Query\Join;
use Uu324\Task\Users\UsersTable;


/**
 * Class DataTable
 *
 * @package \Uu324\Data
 **/

class TasksTable extends Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'uu324_tasks';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('DATA_ENTITY_ID_FIELD'),
            ),
            'USER' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('DATA_ENTITY_USER_FIELD'),
            ),
            'TITLE' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('DATA_ENTITY_TITLE_FIELD'),
            ),
            'SORT' => array(
                'data_type' => 'integer',
                'title' => Loc::getMessage('DATA_ENTITY_SORT_FIELD'),
            ),
            'CREATED' => array(
                'data_type' => 'datetime',
                'title' => Loc::getMessage('DATA_ENTITY_CREATED_FIELD'),
            ),
            (new Reference(
                'USERS',
                UsersTable::class,
                Join::on('this.USER', 'ref.ID')
            ))
                ->configureJoinType('inner')
        );
    }

    public static function getObjectClass()
    {
        return TasksObject::class;
    }

    public static function getCollectionClass()
    {
        return TasksCollection::class;
    }
}