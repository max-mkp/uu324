<?php
namespace Uu324\Task\Users;

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ORM\Fields\Relations\OneToMany;
use Uu324\Task\Tasks\TasksTable;


/**
 * Class DataTable
 *
 * @package \Uu324\Data
 **/

class UsersTable extends Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'uu324_users';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('DATA_ENTITY_ID_FIELD'),
            ),
            'LOGIN' => array(
                'data_type' => 'text',
                'required' => true,
                'title' => Loc::getMessage('DATA_ENTITY_LOGIN_FIELD'),
            ),
            'FIRST_NAME' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('DATA_ENTITY_FIRST_NAME_FIELD'),
            ),
            'LAST_NAME' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('DATA_ENTITY_LAST_NAME_FIELD'),
            ),
            'SECOND_NAME' => array(
                'data_type' => 'text',
                'title' => Loc::getMessage('DATA_ENTITY_SECOND_NAME_FIELD'),
            ),
            (new OneToMany('TASKS', TasksTable::class, 'USERS'))->configureJoinType('inner')
        );
    }
}